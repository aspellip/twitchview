**Simple program by python for watching favorite twitch channels**

.. image:: https://gitlab.com/aspellip/twitchview/badges/master/build.svg 
    :target: https://gitlab.com/aspellip/twitchview/commits/master
.. image:: https://badge.fury.io/py/twitchview.svg
    :target: https://badge.fury.io/py/twitchview

Depends:
 * vlc - http://www.videolan.org/vlc/
 * m3u8 - https://github.com/globocom/m3u8
 * Pillow - https://github.com/python-pillow/Pillow

**Quick Install**

::

 Debian/Ubuntu
    sudo apt install vlc pip python-tk
    sudo pip3 install twitchview

::

 Win
    Download and install Python, VLC
    pip3 install twitchview

::

 MaxOS X
    sudo pip3 install twitchview



Python_ 3.0 or latest package

.. _Python: https://www.python.org/downloads/